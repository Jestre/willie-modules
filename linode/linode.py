"""
linode.py - Fetch Linode Status Updates

Copyright 2016, Scott Schulz, scottschulz.us

Licensed under the Eiffel Forum License 2.

http://sopel.chat

REQUIREMENTS: requests, beautifulsoup4
"""

import requests
from bs4 import BeautifulSoup as BS

from sopel.module import commands, example

url = 'http://status.linode.com/'

"""
Note to self:  Python only uses the default value to the argument
when the function is defined.  Future calls will have maintained
the state, so either reset it through some other means, or
catch errors, e.g. in this case, a TypeError.
"""
def get_limit(arg=1):
    try:
        limit = abs(int(arg))
    except (ValueError, TypeError):
        limit = 1
    if limit > 5:
        limit = 5
    return limit

@commands('linode')
@example('.linode or .linode <number> < 5')
def linode(bot, trigger):
    """ .linode -- Fetch Linode status updates  """

    limit = get_limit(trigger.group(2))
    
    r = requests.get(url)
    if r.status_code == 200:
        soup = BS(r.text, 'html.parser')
        if limit > 1:
            logs = [l.text for l in soup.find_all('div', 'update', limit=limit)]
            #  Change the order to oldest first
            logs.reverse()
            for log in logs:
                bot.say(log)
        else:
            bot.say(soup.find('div', 'update').text)
    else:
        bot.say('Something went wrong, please try again later.')
