"""
ss_bible-esv.py - Willie Bible Verse Lookup

Copyright 2015, Scott Schulz, scottschulz.us
Licensed under the Eiffel Forum License 2.

http://sopel.chat
"""

import re
import requests

#from sopel import web
from sopel.module import commands, example

@commands('bible')
@example('.bible John 3:16 or Rom 3:12-14')
def bible(bot, trigger):
    """ .bible - Look up a bible verse using ESV API. """

    # Check Input
    if not trigger.group(2):
        return bot.reply(u'Sorry, but I need a verse to search for.')

    #  API endpoint
    url = 'http://www.esvapi.org/v2/rest/passageQuery'

    payload = {
        'passage': trigger.group(2), 
        'include-short-copyright': 0, 
        'output-format': 'plain-text', 
        'include-passage-horizontal-lines': 0, 
        'include-heading-horizontal-lines': 0, 
        'key': 'IP', 
        'include-passage-references': 0, 
        'include-headings': 0, 
        'include-subheadings': 0, 
        'include-footnotes': 0,
        'line-length': 0
    }


    r = requests.get(url, params=payload)

    if r.status_code == 200:
        word = re.findall(r'(\[[\d:]+\])([^\[]+)', r.text)
        if len(word) > 0:
            for verse, text in word:
                bot.say(u' '.join([verse, text]))
        else:
            bot.say(u'Sorry, I was unable to understand the results of the query.')
    else:
        bot.say(u'Nothing found... Sorry')

    return
