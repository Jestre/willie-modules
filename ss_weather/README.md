## ss_weather.py

A Sopel module to display weather for a given location.  Provides, in addition to the current weather, both forecast weather
and any watches or warnings.

### Prerequisites

This module uses the excellent Aeris Weather API provided by [HAMweather](http://www.hamweather.com).  A basic (developer) account is free, but provides a limited subset of data and a limited number of accesses.  If you will be running this in a busy channel, please register for one of the paid accounts.

Regardless of which account type you use, you will need to register for an account, and then register an App within that account.  Once the app is configured on the Hamweather website, you will be presented with a set of credentials: 1) The Consumer ID, and 2) The Consumer Secret.  These are the credentials you will need to pass to the bot when configuring this module.

### Setup

1. Copy the .py script to your bot's `modules` directory, typically ~/.sopel/modules.
1. Run `sopel --configure-modules` and when it reaches the configuration options for this module, simply paste in the credentials from the Prerequisites section.
1.  If the bot is already running, tell it to `.load ss_weather` to bring in the new module.  Otherwise, it should be loaded automatically when you next start up the bot.
1. See the README file in the module directory for any setup instructions specifically for that module.

### Usage 

In your channel, or via message, enter `.wx some_location`.  The API recognizes zip codes, city locations, and more.  See the [Aeris Documentation](http://www.hamweather.com/support/documentation/), or specifically the [Aeris Supported Places Docs](http://www.hamweather.com/support/documentation/aeris/supported-places/) for more details.

### Examples

* .wx Raleigh, NC
* .wx 27601
* .wx 37.25,-97.25 (Lat, Long)
* and more

*Simply substitute `.forecast` or `.warnings` in place of `.wx` above to query for those data types.*

### Forthcoming

Right now the bot simply checks for results or not.  In the future it should be able to tell you with more specificity why the search failed (if it did).
