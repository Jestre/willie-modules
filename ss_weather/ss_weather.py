"""
ss_weather.py - Sopel Weather Module

Provides: Current Weather, 3-day Forecast, Warnings

Copyright 2014-2015, Scott Schulz, scottschulz.us
Licensed under the Eiffel Forum License 2.

http://sopel.chat
"""

import json
import urllib
import urllib2

#from sopel import web
from sopel.config.types import StaticSection, ValidatedAttribute
from sopel.module import commands, example

class ssWeatherSection(StaticSection):
    consumer_id = ValidatedAttribute('consumer_id')
    consumer_secret = ValidatedAttribute('consumer_secret')

def setup(bot):
    bot.config.define_section('ss_wxapi', ssWeatherSection)


def configure(config):
    config.define_section('ss_wxapi', ssWeatherSection)
    config.ss_wxapi.configure_setting(
        'consumer_id',
        "Enter the OAuth Consumer ID from your API access account"
    )
    config.ss_wxapi.configure_setting(
        'consumer_secret',
        "Enter the OAuth Consumer Secret from your API access account"
    )

def get_current_wind(ob):
    wind = 'Wind: %s mph' % ob['windSpeedMPH']
    if ob['windSpeedMPH'] > 0:
        wind = wind + ' (%s)' % ob['windDir']
        if ob['windGustMPH'] is not None:
            wind = wind + ', Gusts: %s' % ob['windGustMPH']
    return wind

def get_current_snow(ob):
    if ob['snowDepthIN'] is not None:
        return 'Snow Depth: %s inches. ' % ob['snowDepthIN']
    return ''

def get_forecast_wind(f):
    wind = u'  Wind: %d mph' % f['windSpeedMPH']
    if f['windSpeedMPH'] > 0:
        wind += u' (%s)' % f['windDir']

    if f['windSpeedMaxMPH'] > f['windSpeedMinMPH'] > 0:
        wind += (u', variable from %s @ %d to %s @ %d' % 
            (f['windDirMin'], f['windSpeedMinMPH'], f['windDirMax'], f['windSpeedMaxMPH']))

    if f['windGustMPH'] is not None:
        wind += u', Gusts to %dmph' % f['windGustMPH']
 
    return wind + u'.'

def get_forecast_precip(f):
    if f['snowIN'] > 0:
        return u', %d%% chance of %.2f in. of snow.' % (f['pop'], f['snowIN'])
    elif f['precipIN'] > 0:
        return u', %d%% chance of %.2f in. of rain.' % (f['pop'], f['precipIN'])
    else:
        return u'.'

def get_forecast_pressure(f):
    return (u'  Pressure: %.2f inches (%d hPa).' %
        (f['pressureIN'], f['pressureMB']))

def get_forecast_temps(f):
    return (u'  Temps: High: %d\u00B0F, Low: %d\u00B0F, Average: %d\u00B0F.  Humidity: %d%%, Dew Point: %d\u00B0F.' %
        (f['maxTempF'], f['minTempF'], f['avgTempF'], f['humidity'], f['dewpointF']))

def get_suntimes(ob):
    return 'Sunrise: %s, Sunset: %s. ' % (ob['sunriseISO'][11:16], ob['sunsetISO'][11:16])

def get_time(timeISO):
    return u'%s UTC%s%s' % (timeISO[11:16], timeISO[19], timeISO[20:22].lstrip('0'))

@commands('wx')
@example('.wx Raleigh, NC or .weather KRDU')
def wx(bot, trigger):
    """ .wx - Show any available NWS Weather observations for a location. """

    # Check Input
    if not trigger.group(2):
        return bot.reply(u'Sorry, but I need a location to search.')

    #  Location to search for warnings, from user
    loc = urllib.quote_plus(trigger.group(2))

    #  API endpoint
    url = 'http://api.aerisapi.com/observations/%s?client_id=%s&client_secret=%s' % (
        loc, bot.config.ss_wxapi.consumer_id, bot.config.ss_wxapi.consumer_secret
    )

    obs = json.load(urllib2.urlopen(url))

    if not 'success' in obs or not obs['success']:
        return bot.say(u'Sorry, I didn\'t find any observations for that location.')

    if obs['error'] is not None:
        if obs['error']['code'] == 'warn_no_data':
            return bot.say(u'No observations available for %s' % trigger.group(2))
        else:
            return bot.say(u'Unknown error: ' + obs['error']['description'])
    else:
        ob = obs['response']['ob']
        output = (u'As of %s at %s: %s, Temp: %s\u00B0F, Humidity: %s%%, Dew Point: %s\u00B0F.  %s.  Pressure: %s inches, %s hPa.  Visibility: %s miles. %s %s' %
                (
                    get_time(ob['dateTimeISO']),
                    trigger.group(2),
                    ob['weather'],
                    ob['tempF'],
                    ob['humidity'],
                    ob['dewpointF'],
                    get_current_wind(ob),
                    ob['pressureIN'],
                    ob['pressureMB'],
                    ob['visibilityMI'],
                    get_current_snow(ob),
                    get_suntimes(ob)
                ))
        return bot.say(output)

@commands('forecast')
@example('.forecast Raleigh, NC or .forecast KRDU')
def forecast(bot, trigger):
    """ .forecast - Show NWS Weather forecast for a location. """

    # Check Input
    if not trigger.group(2):
        return bot.reply(u'Sorry, but I need a location to search.')

    #  Location to search for warnings, from user
    loc = urllib.quote_plus(trigger.group(2))

    #  API endpoint
    url = 'http://api.aerisapi.com/forecasts/%s?client_id=%s&client_secret=%s' % (
        loc, bot.config.ss_wxapi.consumer_id, bot.config.ss_wxapi.consumer_secret
    )

    forecasts = json.load(urllib2.urlopen(url))

    #  Check if key exists and if True (avoids KeyError)
    if not 'success' in forecasts or not forecasts['success']:
        return bot.say(u'Sorry, I didn\'t find any forecast for that location.')

    #  error is null if no errors
    if forecasts['error'] is not None:
        if forecasts['error']['code'] == 'warn_no_data':
            return bot.say(u'No forecast available for %s' % trigger.group(2))
        else:
            return bot.say(u'Unknown error: ' + forecasts['error']['description'])
    else:
        days = ['Today', 'Tomorrow', 'Day 3']
        bot.say('Forecast for %s:' % trigger.group(2))
        #  Limit to first three (3) days
        for index in range(3):
            f = forecasts['response'][0]['periods'][index]
            bot.say(u'%s: %s%s %s %s %s %s ' % 
                (days[index], f['weather'], get_forecast_precip(f), 
                 get_forecast_temps(f), get_forecast_wind(f), get_forecast_pressure(f), get_suntimes(f)))
        return

@commands('warnings')
@example('.warnings Raleigh, NC or .warnings 27529')
def warnings(bot, trigger):
    """ .warnings - Show any available NWS Weather warnings for a location. """

    # Check Input
    if not trigger.group(2):
        return bot.reply(u'Sorry, but I need a location to search.')

    #  Location to search for warnings, from user
    loc = urllib.quote_plus(trigger.group(2))

    #  API endpoint
    url = 'http://api.aerisapi.com/advisories/%s?client_id=%s&client_secret=%s' % (
        loc, bot.config.ss_wxapi.consumer_id, bot.config.ss_wxapi.consumer_secret
    )

    warning = json.load(urllib2.urlopen(url))

    if not 'success' in warning or not warning['success']:
        return bot.say(u'Sorry, failed to retrieve any data')

    if warning['error'] is not None:
        if warning['error']['code'] == 'warn_no_data':
            return bot.say(u'No warnings for %s' % trigger.group(2))
        else:
            return bot.say(u'Unknown error: ' + warning['error']['description'])
    else:
        for index, response in enumerate(warning['response']):
            output = (u'%s for %s, %s - Issued: %s, Expires: %s' % 
                (
                    response['details']['name'],
                    response['place']['name'],
                    response['place']['state'],
                    response['timestamps']['issuedISO'],
                    response['timestamps']['expiresISO']
                ))
            bot.say(output)
        return bot.say(u'End of results.')

