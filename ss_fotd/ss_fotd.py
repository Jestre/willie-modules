"""
ss_fotd.py - Willie Flavor of the Day Lookup

Copyright 2015, Scott Schulz, scottschulz.us
Licensed under the Eiffel Forum License 2.

http://willie.dftba.net
"""

import re
import sqlite3

#from willie import web
from willie.module import commands, example

def run_query(cursor, search = None):

    error_string = u"I'm sorry, but I am unable to find the answer you are looking for..."

    if search is None:
        cursor.execute("SELECT flavor FROM fotd WHERE date = date('NOW', '-4 hours')")
        result = cursor.fetchone()
        if result is not None:
            return u"The flavor of the day is: {}".format(result[0])
        else:
            return error_string

    found_date = re.search('\d{4}-\d{2}-\d{2}', search)
    if found_date is not None:
        search_date = ( found_date.group(0), )
        cursor.execute("SELECT flavor FROM fotd WHERE date = ?", search_date)
        result = cursor.fetchone()
        if result is not None:
            return u"The flavor for {} is {}".format(search,  result[0])
        else:
            return error_string

    if len(search) > 3:
        query_term = ('%{}%'.format(search), )
        cursor.execute("SELECT date, flavor FROM fotd WHERE date >= date('NOW', '-4 hours') AND flavor LIKE ?", query_term)
        result = cursor.fetchone()
        if result is not None:
            return u"The next date for a flavor matching '{}' is {}: {}".format(search, result[0], result[1])
        else:
            return error_string

    return error_string


@commands('fotd')
@example('.fotd or .fotd pecan or .fotd _date_')
def fotd(bot, trigger):
    """ .fotd - Look up the Goodberry's Flavor of the Day. """

    fotd_db = "fotd.db"

    conn = sqlite3.connect(fotd_db)
    cur = conn.cursor()

    result = run_query(cur, trigger.group(2))

    conn.close()
    
    if result is not None:
        return bot.say(result)
